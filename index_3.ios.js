import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';

class MeuComponente extends Component {
  render() {
    return (
      <View>
        <Text>{this.props.teste}</Text>
      </View>
    );
  }
}

class app3 extends Component {
  constructor(props) {
    super(props);

    this.state = { texto: 'TExto teste 2' };
  }

  alteraTexto() {
    this.setState({ texto: 'Outro coisa ' });
  }

  render() {
    return (
      <View>
        <MeuComponente teste={this.state.texto} />
        <Button 
          title='Botão'
          onPress={() => { this.alteraTexto(); }}
        />
      </View>
    );
  }
}

AppRegistry.registerComponent('app3', () => app3);
